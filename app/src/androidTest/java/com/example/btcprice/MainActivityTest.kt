package com.example.btcprice

import android.content.Context
import android.content.pm.ActivityInfo
import android.content.res.Configuration
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.swipeLeft
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isChecked
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@LargeTest
class MainActivityTest {

    @Rule
    @JvmField
    val activityRule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun testFilterStatesAfterRotation() {
        val filters = ArrayList<Int>()
        filters.add(R.id.filter_chip_week)
        filters.add(R.id.filter_chip_month)
        filters.add(R.id.filter_chip_two_months)
        filters.add(R.id.filter_chip_three_months)
        filters.add(R.id.filter_chip_year)

        for (viewResource in filters) {
            clickOn(viewResource)
            rotateScreen()
            isSelected(viewResource)
            swipeFilter()
        }
    }

    private fun isSelected(viewResource: Int) {
        onView(withId(viewResource)).check(matches(isChecked()))
    }

    private fun clickOn(viewResource: Int) {
        onView(withId(viewResource)).perform(click())
    }

    private fun swipeFilter() {
        onView(withId(R.id.filters)).perform(swipeLeft())
    }

    private fun rotateScreen() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        val orientation = context.resources.configuration.orientation

        val activity = activityRule.activity
        activity.requestedOrientation = if (orientation == Configuration.ORIENTATION_PORTRAIT)
            ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
        else
            ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
    }

}