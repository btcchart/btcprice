package com.example.btcprice.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.btcprice.api.TimeSpan
import com.example.btcprice.model.Chart
import com.example.btcprice.repository.BlockchainRepository
import com.example.btcprice.utils.RxSchedulerRule
import com.example.btcprice.utils.Status
import com.example.btcprice.utils.mock
import io.reactivex.Observable
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito.*

@RunWith(JUnit4::class)
class ChartViewModelTest {

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    @Rule
    @JvmField
    val rxSchedulerRule = RxSchedulerRule()

    private val repository = mock(BlockchainRepository::class.java)
    private lateinit var viewModel: ChartViewModel

    @Before
    fun init() {
        viewModel = ChartViewModel(repository)
    }

    @Test
    fun notifySuccessCall() {
        viewModel.blockchainData.observeForever(mock())
        val mockChart = mock(Chart::class.java)
        `when`(repository.getChart(anyString())).thenReturn(Observable.just(mockChart))
        viewModel.setTimeSpan(TimeSpan.WEEK)
        val liveData = viewModel.blockchainData.value

        Assert.assertEquals(liveData?.data, mockChart)
        Assert.assertEquals(liveData?.status, Status.SUCCESS)
    }

    @Test
    fun notifyFailureCall() {
        viewModel.blockchainData.observeForever(mock())
        `when`(repository.getChart(anyString())).thenReturn(Observable.error(Throwable("Error")))
        viewModel.setTimeSpan(TimeSpan.WEEK)
        val liveData = viewModel.blockchainData.value

        Assert.assertEquals(liveData?.data, null)
        Assert.assertEquals(liveData?.status, Status.ERROR)
    }

    @Test
    fun callRepoForDifferentTimeSpans() {
        viewModel.blockchainData.observeForever(mock())
        val observable = Observable.just(mock(Chart::class.java))
        val week = TimeSpan.WEEK
        `when`(repository.getChart(week.timeSpanQuery)).thenReturn(observable)
        viewModel.setTimeSpan(week)

        verify(repository).getChart(week.timeSpanQuery)

        reset(repository)

        val year = TimeSpan.YEAR
        `when`(repository.getChart(year.timeSpanQuery)).thenReturn(observable)
        viewModel.setTimeSpan(year)

        verify(repository).getChart(year.timeSpanQuery)
    }

    @Test
    fun avoidDuplicateCallForSameTimeSpan() {
        viewModel.blockchainData.observeForever(mock())
        val observable = Observable.just(mock(Chart::class.java))
        val timeSpan = TimeSpan.WEEK
        `when`(repository.getChart(timeSpan.timeSpanQuery)).thenReturn(observable)
        viewModel.setTimeSpan(timeSpan)
        viewModel.setTimeSpan(timeSpan)

        verify(repository, times(1)).getChart(timeSpan.timeSpanQuery)
    }

    @Test
    fun neverCallForInvalidTimeSpan() {
        viewModel.blockchainData.observeForever(mock())
        val observable = Observable.just(mock(Chart::class.java))
        `when`(repository.getChart(anyString())).thenReturn(observable)
        viewModel.setTimeSpan(mock(TimeSpan::class.java))

        verify(repository, never()).getChart(anyString())
    }

    @Test
    fun neverNotifyForInvalidTimeSpan() {
        viewModel.blockchainData.observeForever(mock())
        val observable = Observable.just(mock(Chart::class.java))
        `when`(repository.getChart(anyString())).thenReturn(observable)
        viewModel.setTimeSpan(TimeSpan.MONTH)

        val liveData = viewModel.blockchainData.value

        Assert.assertEquals(liveData?.status, Status.SUCCESS)

        val invalidTimeSpan = mock(TimeSpan::class.java)
        `when`(invalidTimeSpan.timeSpanQuery).thenReturn("")

        viewModel.setTimeSpan(invalidTimeSpan)

        `when`(invalidTimeSpan.timeSpanQuery).thenReturn(null)

        viewModel.setTimeSpan(invalidTimeSpan)

        Assert.assertEquals(liveData?.status, Status.SUCCESS)
    }
}