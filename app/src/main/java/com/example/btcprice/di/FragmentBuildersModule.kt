package com.example.btcprice.di

import com.example.btcprice.ui.ChartFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class FragmentBuildersModule {

    @ContributesAndroidInjector
    abstract fun contributeChartFragment(): ChartFragment
}