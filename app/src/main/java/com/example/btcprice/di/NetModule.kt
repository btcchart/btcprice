package com.example.btcprice.di

import com.example.btcprice.BuildConfig
import com.example.btcprice.api.BlockchainService
import com.facebook.stetho.okhttp3.StethoInterceptor
import dagger.Module
import dagger.Provides
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

private const val BASE_URL = "https://api.blockchain.info/"

@Suppress("unused")
@Module
class NetModule {

    @Singleton
    @Provides
    fun provideNetworkInterceptor(): Interceptor? = if (BuildConfig.DEBUG) StethoInterceptor() else null

    @Singleton
    @Provides
    fun provideOkHttpClient(interceptor: Interceptor?): OkHttpClient {
        val httpClient = OkHttpClient.Builder()
        interceptor?.let { httpClient.addNetworkInterceptor(it) }
        return httpClient.build()
    }

    @Singleton
    @Provides
    fun provideBlockchainService(okHttpClient: OkHttpClient): BlockchainService {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(okHttpClient)
            .build()
            .create(BlockchainService::class.java)
    }
}