package com.example.btcprice.ui

import android.os.Bundle
import android.text.format.DateUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.example.btcprice.R
import com.example.btcprice.api.TimeSpan
import com.example.btcprice.di.Injectable
import com.example.btcprice.model.Chart
import com.example.btcprice.utils.Status
import com.example.btcprice.viewmodel.ChartViewModel
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.IAxisValueFormatter
import com.google.android.material.chip.ChipGroup
import kotlinx.android.synthetic.main.fragment_chart_view.*
import javax.inject.Inject

class ChartFragment : Fragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var chartViewModel: ChartViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_chart_view, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        chartViewModel = ViewModelProviders.of(this, viewModelFactory).get(ChartViewModel::class.java)

        setupChart(chart)
        setupFilters(filterGroup)
        setupDataObserver(chart, progressBar)
    }

    private fun setupChart(lineChart: LineChart) {
        lineChart.setNoDataText(getString(R.string.empty_data))
        lineChart.setNoDataTextColor(ContextCompat.getColor(lineChart.context, R.color.colorAccent))
        lineChart.setPinchZoom(false)
        lineChart.setDrawGridBackground(false)
        lineChart.isAutoScaleMinMaxEnabled = false
        lineChart.xAxis?.setDrawGridLines(false)
        lineChart.xAxis?.granularity = (DateUtils.DAY_IN_MILLIS / DateUtils.SECOND_IN_MILLIS).toFloat()
        lineChart.xAxis?.valueFormatter = IAxisValueFormatter { value, _ ->
            DateUtils.formatDateTime(lineChart.context, value.toLong() * DateUtils.SECOND_IN_MILLIS, DateUtils.FORMAT_ABBREV_MONTH or DateUtils.FORMAT_NO_YEAR)
        }
        lineChart.invalidate()
    }

    private fun setupFilters(chipGroup: ChipGroup) {
        chipGroup.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                R.id.filter_chip_week -> chartViewModel.setTimeSpan(TimeSpan.WEEK)
                R.id.filter_chip_month -> chartViewModel.setTimeSpan(TimeSpan.MONTH)
                R.id.filter_chip_two_months -> chartViewModel.setTimeSpan(TimeSpan.TWO_MONTHS)
                R.id.filter_chip_three_months -> chartViewModel.setTimeSpan(TimeSpan.THREE_MONTHS)
                R.id.filter_chip_year -> chartViewModel.setTimeSpan(TimeSpan.YEAR)
            }
        }
    }

    private fun setupDataObserver(lineChart: LineChart, progressBar: ProgressBar) {
        chartViewModel.blockchainData.observe(this, Observer { response ->
            when {
                response?.status == Status.SUCCESS -> {
                    val chartData = response.data
                    progressBar.visibility = View.GONE
                    updateChartData(lineChart, chartData)
                }
                response?.status == Status.LOADING -> {
                    progressBar.visibility = View.VISIBLE
                }
                response?.status == Status.ERROR -> {
                    progressBar.visibility = View.GONE
                    Toast.makeText(requireContext(), R.string.error, Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

    private fun updateChartData(lineChart: LineChart, newChartData: Chart?) {
        val dataSetIndex = 0
        val chartData = lineChart.data
        chartData?.let { lineChart.data?.clearValues() } ?: run { //Reuse the chart data if it already exists. Otherwise, create a new one.
            lineChart.data = LineData()
        }
        val chartDataSet = lineChart.data?.getDataSetByIndex(dataSetIndex)
        chartDataSet ?: run { //Reuse the data set if it already exists. Otherwise, create a new one.
            lineChart.data?.addDataSet(createChartDataSet(newChartData?.name))
        }

        newChartData?.values.orEmpty().forEach { value ->
            value.x?.let {
                lineChart.data?.addEntry(Entry(value.x.toFloat(), value.y ?: 0f), dataSetIndex)
            }
        }

        lineChart.description?.text = newChartData?.description
        lineChart.data?.notifyDataChanged()
        lineChart.notifyDataSetChanged()
        lineChart.fitScreen()
        lineChart.resetZoom()
    }

    private fun createChartDataSet(setName: String?): LineDataSet {
        val dataSet = LineDataSet(null, setName)
        dataSet.setDrawCircles(false)
        dataSet.lineWidth = requireContext().resources.getDimension(R.dimen.chart_line_width)
        dataSet.color = ContextCompat.getColor(requireContext(), R.color.colorAccent)
        return dataSet
    }
}

