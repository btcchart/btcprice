package com.example.btcprice.repository

import com.example.btcprice.api.BlockchainService
import com.example.btcprice.model.Chart
import io.reactivex.Observable
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class BlockchainRepository @Inject constructor(private val blockchainService: BlockchainService) {

    fun getChart(timeSpan: String): Observable<Chart> = blockchainService.getChart(timeSpan)

}
