package com.example.btcprice.model

import com.google.gson.annotations.SerializedName

data class Chart(
    @SerializedName("description") val description: String?,
    @SerializedName("name") val name: String?,
    @SerializedName("values") val values: List<Value>?
)

data class Value(
    @SerializedName("x") val x: Long?,
    @SerializedName("y") val y: Float?
)