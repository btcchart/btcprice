package com.example.btcprice.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.example.btcprice.api.TimeSpan
import com.example.btcprice.model.Chart
import com.example.btcprice.repository.BlockchainRepository
import com.example.btcprice.utils.Resource
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class ChartViewModel
@Inject constructor(private val blockchainRepository: BlockchainRepository) : ViewModel() {

    private val disposables = CompositeDisposable()
    private val _timeSpanQuery = MutableLiveData<String>()
    private val _blockchainData = MutableLiveData<Resource<Chart>>()

    val blockchainData: LiveData<Resource<Chart>> = Transformations.switchMap(_timeSpanQuery) { timeSpanQuery ->
        if (timeSpanQuery.isNullOrBlank()) {
            MutableLiveData()
        } else {
            loadChart(timeSpanQuery)
            _blockchainData
        }
    }

    private fun loadChart(timeSpanQuery: String) {
        disposables.add(blockchainRepository.getChart(timeSpanQuery)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { _blockchainData.setValue(Resource.loading(null)) }
            .subscribe(
                { result -> _blockchainData.setValue(Resource.success(result)) },
                { throwable -> _blockchainData.setValue(Resource.error(throwable.message)) }
            )
        )
    }

    fun setTimeSpan(timeSpan: TimeSpan) {
        if (timeSpan.timeSpanQuery == _timeSpanQuery.value) {
            return
        }
        _timeSpanQuery.value = timeSpan.timeSpanQuery
    }

    override fun onCleared() {
        disposables.clear()
    }
}
