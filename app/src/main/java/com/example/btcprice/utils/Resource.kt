package com.example.btcprice.utils

data class Resource<out T>(val status: Status, val data: T?, val message: String?) {
    companion object {
        fun <T> loading(data: T?): Resource<T> {
            return Resource(Status.LOADING, data, null)
        }

        fun <T> success(data: T?): Resource<T> {
            return Resource(Status.SUCCESS, data, null)
        }

        fun <T> error(msg: String? = "unknown error"): Resource<T> {
            return Resource(Status.ERROR, null, msg)
        }
    }
}

enum class Status {
    LOADING,
    SUCCESS,
    ERROR
}

