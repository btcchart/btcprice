package com.example.btcprice.api

import com.example.btcprice.model.Chart
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface BlockchainService {

    @GET("charts/market-price?format=json")
    fun getChart(@Query("timespan") timeSpan: String): Observable<Chart>
}

enum class TimeSpan(val timeSpanQuery: String) {
    WEEK("7days"),
    MONTH("30days"),
    TWO_MONTHS("60days"),
    THREE_MONTHS("180days"),
    YEAR("1year")
}